var CardGenerator = require('../../card-generation.js').CardGenerator,
	CardValidator = require('../../card-validation.js').CardValidator,
	chai = require('chai');
	
describe("should test card generation", function () {
	it ("should test CardGenerator exits", function () {
		chai.expect(CardGenerator).not.to.be.null;
		chai.expect(CardGenerator).not.to.be.undefined;
		chai.expect(CardGenerator).to.be.a("Object");
	});
	it ("should test genRandNumString", function () {
		
		chai.expect(CardGenerator).to.have.property('genRandNumString');
		chai.expect(CardGenerator.genRandNumString(0).length).to.equal(0);
		chai.expect(CardGenerator.genRandNumString(0)).to.be.a('string');
		chai.expect(CardGenerator.genRandNumString(1).length).to.equal(1);
		chai.expect(CardGenerator.genRandNumString(2).length).to.equal(2);
		chai.expect(CardGenerator.genRandNumString(2)).to.be.a('string');
		
		var num = CardGenerator.genRandNumString(10), i;
		for (i = 0; i < 10; i += 1) {
			var tmp = parseInt(num.substr(i, 1));
			chai.expect((tmp >= 0) && (tmp < 10)).to.be.true;
		}
		
	});
	it ("should test preLuhn", function () {
		var i, str, len;
		for (i = 0; i < 1000; i += 1) {
			len = Math.floor(Math.random() * 20);
			str = CardGenerator.genRandNumString(len);
			chai.expect(CardValidator.luhn(str + CardGenerator.preLuhn(str))).to.be.true;
		}
	});
	
});





























