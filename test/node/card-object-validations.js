﻿var CardValidator = require('../../card-validation.js').CardValidator,
	chai = require('chai');
	
describe("should test exported CardValidator object", function () {
	
	it ("should check instance of", function () {
		chai.expect(CardValidator).not.to.be.null;
		chai.expect(CardValidator).not.to.be.undefined;
	});
	
	it ("should check consistency of CardValidator", function () {
		chai.expect(CardValidator).to.have.property('luhn');
		chai.expect(CardValidator).to.have.property('getNums');
		chai.expect(CardValidator).to.have.property('firstMatch');
		chai.expect(CardValidator).to.have.property('isValidCard');
		chai.expect(CardValidator).to.have.property('matchUSZip');
		chai.expect(CardValidator).to.have.property('extractUSZip');
		chai.expect(CardValidator).to.have.property('matchCanadaPostal');
		chai.expect(CardValidator).to.have.property('extractCanadaPostal');
		chai.expect(CardValidator).to.have.property('cards').to.be.a('object');
		chai.expect(CardValidator).to.have.property('cardsOrder').to.be.a('array');
	});
	
	it ("should check various cards against", function () {
		var cards = [
			{
				name: "Visa",
				num: "4242424242424242"
			},
			{
				name: "Visa Electron",
				num: "4175000777040586"
			},
			{
				name: "Visa",
				num: "4175000777040586"
			}
		], i;
		for (i = 0; i < cards.length; i += 1) {
			var res = CardValidator.isValidCard(cards[i].name)(cards[i].num);
			chai.expect(res).to.be.a('array');
			chai.expect(res[0]).to.be.true;
			chai.expect(res[1]).to.be.true;
		}
	});
	
	it ("should check luhn (README examples)", function () {
		chai.expect(CardValidator.luhn("4242424242424242")).to.be.true;
		chai.expect(CardValidator.luhn("4242 4242 4242 4242")).to.be.true;
		chai.expect(CardValidator.luhn(4242424242424242)).to.be.true;
		chai.expect(CardValidator.luhn("4242 oh my ... 424242424242🐹")).to.be.true;
		
		chai.expect(CardValidator.luhn("4242424242424243")).to.be.false;
		chai.expect(CardValidator.luhn("4242 4242 4242 4243")).to.be.false;
		chai.expect(CardValidator.luhn(4242424242424243)).to.be.false;
		chai.expect(CardValidator.luhn("4242 oh my ... 424242424243🐹")).to.be.false;
	});
	
	it ("should extract numbers from string (REAME  examples)", function () {
		chai.expect(CardValidator.getNums("1234-56!.78*9👊💥0")).to.equal("1234567890");
	});
	
	it ("should matchUSZip numbers", function () {
		chai.expect(CardValidator.matchUSZip("12345")).to.be.true;
		chai.expect(CardValidator.matchUSZip(" 1 2 3 4 5 ")).to.be.true;
		chai.expect(CardValidator.matchUSZip(" 1 2 3 4 5 - 34 67 ")).to.be.true;
		chai.expect(CardValidator.matchUSZip("12345 1234")).to.be.false;
		chai.expect(CardValidator.matchUSZip("12345-1234-")).to.be.false;
		chai.expect(CardValidator.matchUSZip("10022-SHOE")).to.be.true;
		chai.expect(CardValidator.matchUSZip(" 10022 - shoe ")).to.be.true;
		chai.expect(CardValidator.matchUSZip(12345)).to.be.true;
		chai.expect(CardValidator.matchUSZip(02345)).to.be.false;
		chai.expect(CardValidator.matchUSZip("Terminator")).to.be.false;
	});
	
	it ("should match Canada Postal Codes", function () {
		chai.expect(CardValidator.matchCanadaPostal('X0A1A1')).to.be.true;
		chai.expect(CardValidator.matchCanadaPostal(' X0A1A1 ')).to.be.true;
		chai.expect(CardValidator.matchCanadaPostal(' X0A-1A1 ')).to.be.true;
		chai.expect(CardValidator.matchCanadaPostal('X0A 1A1')).to.be.true;
		chai.expect(CardValidator.matchCanadaPostal('x0a 1a1')).to.be.true;
		chai.expect(CardValidator.matchCanadaPostal('x0a🌐1a1')).to.be.true;
		
		chai.expect(CardValidator.matchCanadaPostal('1A11A1')).to.be.false;
		chai.expect(CardValidator.matchCanadaPostal('Z0A1A1')).to.be.false;
		chai.expect(CardValidator.matchCanadaPostal('X0A1A12')).to.be.false;
		chai.expect(CardValidator.matchCanadaPostal('X011O1')).to.be.false;
		chai.expect(CardValidator.matchCanadaPostal()).to.be.false;
		chai.expect(CardValidator.matchCanadaPostal(null)).to.be.false;
		chai.expect(CardValidator.matchCanadaPostal(123456)).to.be.false;
	});
	
});











