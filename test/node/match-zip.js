﻿var _matchUSZip = require('../../card-validation.js')._matchUSZip,
	_matchCanadaPostal = require('../../card-validation.js')._matchCanadaPostal,
	matchCanadaPostal = require('../../card-validation.js').matchCanadaPostal,
	matchUSZip 	= require('../../card-validation.js').matchUSZip,
	extractUSZip = require('../../card-validation.js').extractUSZip;
	extractCanadaPostal = require('../../card-validation.js').extractCanadaPostal;
	SeqPostalGeneratorFabric = require('./canada.postal.samples.js').SeqPostalGeneratorFabric,
	rs 			= require("randomstring"),
	chai 		= require('chai');
	
describe("should check US zip code function", function () {
	it ("should work with bad arguments", function () {
		chai.expect(matchUSZip()).to.be.false;
		chai.expect(matchUSZip(undefined)).to.be.false;
		chai.expect(matchUSZip(null)).to.be.false;
		chai.expect(matchUSZip(true)).to.be.false;
		chai.expect(matchUSZip("")).to.be.false;
	});
	
	it ("should find strange SHOE zip code", function () {
		chai.expect(matchUSZip("10022-SHOE")).to.be.true;
	});
	
	it ("should match many random generated strings of format xxxxx", function () {
		var zips = [], i, options = { length: 5, charset: "numeric" };
		for (i = 0; i < 1000; i += 1) {
			chai.expect(matchUSZip(rs.generate(options))).to.be.true;
		}
	});
	
	it ("should match many random generated strings of format xxxxx-xxxx", function () {
		var zips = [], i, 
			options1 = { length: 5, charset: "numeric" },
			options2 = { length: 4, charset: "numeric" };
		for (i = 0; i < 1000; i += 1) {
			chai.expect(matchUSZip(rs.generate(options1) + "-" + rs.generate(options2) )).to.be.true;
		}
	});
});

describe("should match Canada Postal Codes", function () {
	it ("should go through all postals with random locals", function () {
		var generator = SeqPostalGeneratorFabric(),
			postal;
		while ((postal = generator()) !== null) {
			chai.expect(!!_matchCanadaPostal(postal)).to.be.true;
		}
	});
	it ("should go through all postals with random locals", function () {
		var generator = SeqPostalGeneratorFabric(),
			postal;
		while ((postal = generator()) !== null) {
			chai.expect(matchCanadaPostal(postal)).to.be.true;
		}
	});
	it ("should match some zips", function () {
		var zips = [
			"x0a1S1",
			"x0a-1A1",
			"x0a 1A1",
			" x0a-1A1",
			"x0a!1A1",
			"x0a😰1A1",
		];
		for (var i = 0; i < zips.length; i += 1) {
			chai.expect(matchCanadaPostal(zips[i])).to.be.true;
		}
	});
	it ("should fail some zips", function () {
		var zips = [
			"1H11H1",
			"X0AX0A",
			"W0A1H1",
			"H0H1H10"
		];
		for (var i = 0; i < zips.length; i += 1) {
			chai.expect(!!_matchCanadaPostal(zips[i])).to.be.false;
		}
	});
});

describe("should test extract US ZipCode functions", function () {
	it ("should extract US Zip from shoe strings", function () {
		chai.expect(extractUSZip("10022-SHOE")).to.equal("10022-SHOE");
		chai.expect(extractUSZip(" 10022 - shoe ")).to.equal("10022-SHOE");
		chai.expect(extractUSZip(" 10022 - sHoe! ")).to.equal("10022-SHOE");
		chai.expect(extractUSZip("😹10022-SHOE")).to.equal("10022-SHOE");
	});
	
	it ("should extract US Zip from strings", function () {
		chai.expect(extractUSZip("10022")).to.equal("10022");
		chai.expect(extractUSZip(" 10022 ")).to.equal("10022");
		chai.expect(extractUSZip(" 1x0x0x2x2 ")).to.equal("10022");
		chai.expect(extractUSZip("😹10022!!!")).to.equal("10022");
	});
	
	it ("should match many random generated strings of format xxxxx", function () {
		var zips = [], i, options = { length: 5, charset: "numeric" }, tmp;
		for (i = 0; i < 50; i += 1) {
			tmp = rs.generate(options);
			chai.expect(extractUSZip(tmp)).to.equal(tmp);
		}
	});
	
	it ("should match many random generated strings of format xxxxx-xxxx", function () {
		var zips = [], i, 
			options1 = { length: 5, charset: "numeric" },
			options2 = { length: 4, charset: "numeric" },
			tmp;
		for (i = 0; i < 50; i += 1) {
			tmp = rs.generate(options1) + "-" + rs.generate(options2);
			chai.expect(extractUSZip(tmp)).to.equal(tmp);
		}
	});
});

describe("should test extract Cnada Postals functions", function () {
	it ("should extract some zips", function () {
		var zips = {
			"x0a1S1" : "X0A1S1",
			"x0a-1A1": "X0A1A1",
			"x0a 1A1": "X0A1A1",
			" x0a-1A1": "X0A1A1",
			"x0a!1A1": "X0A1A1",
			"x0a😰1A1": "X0A1A1",
		};
		for (var key in zips) {
			chai.expect(extractCanadaPostal(key)).equal(zips[key]);
		}
	});
	it ("should go through all postals with random locals", function () {
		var generator = SeqPostalGeneratorFabric(),
			postal;
		while ((postal = generator()) !== null) {
			chai.expect(extractCanadaPostal(postal)).to.equal(postal);
		}
	});
});




















