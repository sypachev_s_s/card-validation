﻿var luhn = require('../../card-validation.js').luhn,
	getNums = require('../../card-validation.js').getNums,
	isNumericString = require('../../card-validation.js').isNumericString,
	_firstMatch = require('../../card-validation.js')._firstMatch,
	firstMatch = require('../../card-validation.js').firstMatch,
	isValidCard = require('../../card-validation.js').isValidCard,
	cardsRegexp	= require('../../card-validation.js').cardsRegexp;
	cardsOrder	= require('../../card-validation.js').cardsOrder;
	chai = require('chai');
	
describe("should test consistency of cards", function () {
	it ("should test consistency of all cards", function () {
		for (var key in cardsRegexp) {
			var o = cardsRegexp[key];
			chai.expect(o).to.have.property('name');
			chai.expect(o).to.have.property('family');
			chai.expect(o).to.have.property('aka');
			chai.expect(o).to.have.property('vn');
			chai.expect(o).to.have.property('vc');
			chai.expect(o.aka).to.be.a('array');
			chai.expect(o.name).to.be.a('string');
			chai.expect(o.family).to.be.a('string');
			chai.expect(o.vn).to.be.a('Function');
			chai.expect(o.vc).to.be.a('Function');
		}
	});
	it ("should check that all card names are mirrord in cardsRegexp object", function () {
		var i, name;
		for (i = 0; i < cardsOrder.length; i += 1) {
			name = cardsOrder[i];
			chai.expect(cardsRegexp).to.have.property(name);
		}
	});
});
	
describe("should test isnumericString", function () {
	it ("should test numeric strings", function () {
		var badNums = [
			"",
			"asd asd",
			"123 123",
			"123 ",
			undefined,
			null
		];
		badNums.forEach(function (item) {
			chai.expect(isNumericString(item)).to.be.false;
		});
	});
	it ("should test numeric strings", function () {
		var goodNums = [
			"123",
			"1",
			"4242424242424242"
		];
		goodNums.forEach(function (item) {
			chai.expect(isNumericString(item)).to.be.true;
		});
	});
});
	
describe("should test luhn algorithm", function () {
	it ("should test ok", function () {
		var algsTrue = [
			"4556737586899855",
			"4556669737234288",
			"1234567812345670",
			"4716986054675118",
			"36150699733995",
			"6376308935129767"
		];
		algsTrue.forEach(function (item) {
			var out = luhn(item);
			chai.expect(out).to.be.true;
		});
		
	});
	it ("should test fail", function () {
		var algsFalse = [
			"4556737586899854",
			"4556669737234280",
			"1234567812345677",
			"4716986054675119",
			"36150699733998",
			"6376308935129760"
		];
		algsFalse.forEach(function (item) {
			var out = luhn(item);
			chai.expect(out).to.be.false;
		});
	});
	it ("should fail on bad argument (undefined)", function () {
		chai.expect(luhn()).to.be.false;
	});
	it ("should fail on bad argument (null)", function () {
		chai.expect(luhn(null)).to.be.false;
	});
	it ("should fail on bad argument (true)", function () {
		chai.expect(luhn(true)).to.be.false;
	});
	it ("should fail on bad argument ('hello world')", function () {
		chai.expect(luhn("hello world")).to.be.false;
	});
});

describe ("should test extract numbers from string", function () {
	it ("should return same strings as input", function () {
		chai.expect(getNums("1234567890")).to.equal("1234567890");
		chai.expect(getNums("1234567891")).to.equal("1234567891");
		chai.expect(getNums("1231234567890")).to.equal("1231234567890");
	});
	it ("should extract", function () {
		chai.expect(getNums(" 1234 5678 90-asd")).to.equal("1234567890");
	});
	it ("should extract", function () {
		chai.expect(getNums("1234 5678 9012 2341")).to.equal("1234567890122341");
	});
	it ("should extract with utf8 chars", function () {
		chai.expect(getNums("1234🐹5")).to.equal("12345");
		chai.expect(getNums("🐹58")).to.equal("58");
		chai.expect(getNums("1234🐹")).to.equal("1234");
		chai.expect(getNums("1234🐹123🐹123")).to.equal("1234123123");
	});
});

describe("should test cards' cvv test", function () {
	it ("should test Visa Electron", function () {
		var data = [
			{ name: "Visa Electron", good: ["123"], bad: ["1234", "12", "0123"] },
			{ name: "Visa", good: ["123"], bad: ["1234", "12", "0123"] },
			{ name: "Discover", good: ["123"], bad: ["1234", "12", "0123"] },
			{ name: "American Express", good: ["1234"], bad: ["123", "12455", "01234"] },
			{ name: "Diners Club - Carte Blanch", good: ["123"], bad: ["1234", "12", "0123"] },
			{ name: "Diners Club - International", good: ["123"], bad: ["1234", "12", "0123"] },
			{ name: "JCB", good: ["123"], bad: ["1234", "12", "0123"] },
			{ name: "Laser", good: ["123"], bad: ["1234", "12", "0123"] },
			{ name: "Maestro", good: ["123"], bad: ["1234", "12", "0123"] },
			{ name: "UATP", good: ["123"], bad: ["1234", "12", "0123"] },
			{ name: "UnionPay", good: ["123"], bad: ["1234", "12", "0123"] },
			{ name: "Mastercard", good: ["123"], bad: ["1234", "12", "0123"] },
			{ name: "MIR", good: ["123"], bad: ["1234", "12", "0123"] },
			{ name: "InterPayment", good: ["123"], bad: ["1234", "12", "0123"] },
		], i;
		for (i = 0; i < data.length; i += 1) {
			var vc = cardsRegexp[data[i].name].vc, j;
			for (j = 0; j < data[i].good.length; j += 1) {
				chai.expect(vc(data[i].good[j])).to.be.true;
			}
			for (j = 0; j < data[i].bad.length; j += 1) {
				chai.expect(vc(data[i].bad[j])).to.be.false;
			}
		}
	});
});

describe("should find type of", function () {
	it ("should find Visa", function () {
		var cards16 = [
			"4265038051405332",
			"4265036661512604",
			"4001315702724645"
		];
		var cards13 = [
			"4539425236448",
			"4539694028773",
			"4024007189497",
			"4606776178055",
			"4024007178920",
			"4485724331003"
		];
		cards16.forEach(function (item) {
			var res = _firstMatch(item);
			chai.expect(res).to.equal('Visa');
		});
		cards13.forEach(function (item) {
			var res = _firstMatch(item);
			chai.expect(res).to.equal('Visa');
		});
	});
	it ("should find Visa Electron", function () {
		var cards = [
			"4913327686655684",
			"4175000777040586",
			"4913221554212985",
			"4917853568526897",
			"4917219532421971",
			"4913610472061591"
		];
		cards.forEach(function (item) {
			var res = _firstMatch(item);
			chai.expect(res).to.equal('Visa Electron');
		});
	});
	it ("should find Mastercard", function () {
		var cards = [
			"5590425946410606",
			"5164234501976720",
			"5364206701443402",
			"5503305371106521",
			"5253724869119584",
			"5506244455117426",
			"5572194152798466",
			"5293639141883165",
			"5490031559443032",
			"5271372881712446",
			"5271372881712446",
			"5467885009502088",
			"2221123412341234",
			"2222123412341234",
			"2719123123123123"
		];
		cards.forEach(function (item) {
			var res = _firstMatch(item);
			chai.expect(res).to.equal('Mastercard');
		});
	});
	it ("shoild find Discover", function () {
		var cards = [
			"6582106993496080",
			"6494467833727614",
			"6011542612557517",
			"6011542612557517",
			"6011542612557517",
			"6011542612557517",
			"6229256062789755",
			"6588024333910229",
			"6588024333910229",
			"6588928120488829",
			"6448342213778023",
			"6229252520650044"
		];
		cards.forEach(function (item) {
			var res = _firstMatch(item);
			chai.expect(res).to.equal('Discover');
		});
	});
	it ("should find American Express", function () {
		var cards = [
			"374636548539492",
			"378168256046656",
			"375559186724268",
			"347694400336949",
			"346066611102066",
			"371195964314446",
			"373229285896001",
			"376847032997314",
			"348509821859338",
			"374895579383671"
		];
		cards.forEach(function (item) {
			var res = _firstMatch(item);
			chai.expect(res).to.equal('American Express');
		});
	});
	it ("should find Diners Club - Carte Blanch", function () {
		var cards = [
			"30312191018095",
			"30076627950607",
			"30514125334445",
			"30422040000614",
			"30436559183637",
			"30552102203811",
			"30551642588970",
			"30375445016462"
		];
		cards.forEach(function (item) {
			var res = _firstMatch(item);
			chai.expect(res).to.equal('Diners Club - Carte Blanch');
		});
	});
	it ("should find Diners Club - International", function () {
		var cards = [
			"3860176396516990",
			"3936988495921818",
			"3853764527483559",
			"3854491479975890",
			"3908235880065999",
			"3842696500390050",
			"3925108567367922",
			"36184741310213",
			"36035143574170",
			"36081565028881",
			"36052544248114",
			"36101860424831",
			"36825051837787"
		];
		cards.forEach(function (item) {
			var res = _firstMatch(item);
			chai.expect(res).to.equal('Diners Club - International');
		});
	});
	it("should find JCB", function () {
		var cards = [
			"3528396569034811",
			"3589787147674589",
			"3589430363182470",
			"3589669473351166",
			"3589833282836961",
			"3589280528295133",
			"3589478584168497",
			"3589670367595645",
			"3538028120320631",
			"3538054802204083",
			"3538652701744255",
			"3538657406083867"
		];
		cards.forEach(function (item) {
			var res = _firstMatch(item);
			chai.expect(res).to.equal('JCB');
		});
	});
	it ("should find UnionPay", function () {
		var cards = [
			"6252715933560711",
			"6263677554390637",
			"6244397112156610",
			"6233296874328727",
			"6236571878980047",
			"6265309505489817"
		];
		cards.forEach(function (item) {
			var res = _firstMatch(item);
			chai.expect(res).to.equal('UnionPay');
		});
	});
	it ("should find Visa Dankort", function () {
		var cards = [
			"5019619270367274",
			"5019220823600012",
			"5019946848909836",
			"5019964691923903",
			"5019376378373957",
			"5019848731528998"
		];
		cards.forEach(function (item) {
			var res = _firstMatch(item);
			chai.expect(res).to.equal('Visa Dankort');
		});
	});
	it ("should find UATP", function () {
		var cards = [
			"167356429969384",
			"127138439809449",
			"144902258688658",
			"193421559098996",
			"133678680849539",
			"118612033583577"
		];
		cards.forEach(function (item) {
			var res = _firstMatch(item);
			chai.expect(res).to.equal('UATP');
		});
	});
	
	it ("should find MIR", function () {
		var cards = [
			"2200123412341234",
			"2201123412341234",
			"2202123412341234",
			"2203123412341234",
			"2204123412341234"
		];
		cards.forEach(function (item) {
			var res = _firstMatch(item);
			chai.expect(res).to.equal('MIR');
		});
	});
	
	it ("should find InterPayment", function () {
		var cards = [
			"6360123412341234",
			"63611234123412341",
			"636212341234123412",
			"6363123412341234123",
			"6364123412341234"
		];
		cards.forEach(function (item) {
			var res = _firstMatch(item);
			chai.expect(res).to.equal('InterPayment');
		});
	});
});

describe("should test isValidCard", function () {
	it ("should check Visa", function () {
		var res = isValidCard("Visa")("4242424242424242");
		chai.expect(res).to.be.a('array');
		chai.expect(res[0]).to.be.true;
		chai.expect(res[1]).to.be.true;
	});
	it ("should check Visa, but fail luhn", function () {
		var res = isValidCard("Visa")("4242424242424243");
		chai.expect(res).to.be.a('array');
		chai.expect(res[0]).to.be.false;
		chai.expect(res[1]).to.be.true;
	});
	it ("should check luhn, but fail Visa", function () {
		var res = isValidCard("Visa")("3242424242424244");
		chai.expect(res).to.be.a('array');
		chai.expect(res[0]).to.be.true;
		chai.expect(res[1]).to.be.false;
	});
	it ("should fail luhn, fail Visa", function () {
		var res = isValidCard("Visa")("3242424242424243");
		chai.expect(res).to.be.a('array');
		chai.expect(res[0]).to.be.false;
		chai.expect(res[1]).to.be.false;
	});
	it ("should fail luhn, fail Visa", function () {
		var res = isValidCard("Visa")();
		chai.expect(res).to.be.a('array');
		chai.expect(res[0]).to.be.false;
		chai.expect(res[1]).to.be.false;
	});
	it ("should check luhn, fail undefined card type", function () {
		var res = isValidCard("Robocop")(4242424242424242);
		chai.expect(res).to.be.false;
	});
	it ("should check luhn, check Visa", function () {
		var res = isValidCard("Visa")(4242424242424242);
		chai.expect(res).to.be.a('array');
		chai.expect(res[0]).to.be.true;
		chai.expect(res[1]).to.be.true;
	});
	it ("should check dirty string", function () {
		var res = isValidCard("Visa")("4242 4242 4242 4242");
		chai.expect(res).to.be.a('array');
		chai.expect(res[0]).to.be.true;
		chai.expect(res[1]).to.be.true;
	});
});

describe("should test firstMatch function", function () {
	it ("should fail on bad arguments (undefined, null, number, boolean)", function () {
		chai.expect(firstMatch()).to.be.false;
		chai.expect(firstMatch(null)).to.be.false;
		chai.expect(firstMatch(4242424242424242)).to.be.false;
		chai.expect(firstMatch(true)).to.be.false;
	});
	it ("should not fail", function () {
		chai.expect(function () {
			firstMatch("");
		}).not.to.throw(TypeError);
		chai.expect(function () {
			firstMatch("-");
		}).not.to.throw(TypeError);
		chai.expect(firstMatch("4242424242424242")).to.equal("Visa");
	});
});

describe("should test isValidCard for two arguments", function () {
	it ("should check Luhn, Visa, cvv", function () {
		var res = isValidCard("Visa")("4242424242424242", "123");
		chai.expect(res).to.be.a('array');
		chai.expect(res[0]).to.be.true;
		chai.expect(res[1]).to.be.true;
		chai.expect(res[2]).to.be.true;
	});
	it ("should check Visa, cvv, but fail luhn", function () {
		var res = isValidCard("Visa")("4242424242424243", "123");
		chai.expect(res).to.be.a('array');
		chai.expect(res[0]).to.be.false;
		chai.expect(res[1]).to.be.true;
		chai.expect(res[2]).to.be.true;
	});
	
	it ("should check luhn and cvv, but fail Visa", function () {
		var res = isValidCard("Visa")("3242424242424244", "123");
		chai.expect(res).to.be.a('array');
		chai.expect(res[0]).to.be.true;
		chai.expect(res[1]).to.be.false;
		chai.expect(res[2]).to.be.true;
	});
	
	it ("should check luhn, but fail Visa and cvv", function () {
		var res = isValidCard("Visa")("3242424242424244", "1237");
		chai.expect(res).to.be.a('array');
		chai.expect(res[0]).to.be.true;
		chai.expect(res[1]).to.be.false;
		chai.expect(res[2]).to.be.false;
	});
	
	it ("should fail luhn, fail Visa, check cvv", function () {
		var res = isValidCard("Visa")("3242424242424243", "123");
		chai.expect(res).to.be.a('array');
		chai.expect(res[0]).to.be.false;
		chai.expect(res[1]).to.be.false;
		chai.expect(res[2]).to.be.true;
	});
	
	it ("should fail luhn, fail Visa, cvv", function () {
		var res = isValidCard("Visa")("3242424242424243", "0123");
		chai.expect(res).to.be.a('array');
		chai.expect(res[0]).to.be.false;
		chai.expect(res[1]).to.be.false;
		chai.expect(res[2]).to.be.false;
	});

	it ("should fail luhn, fail Visa, check cvv", function () {
		var res = isValidCard("Visa")(undefined, "123");
		chai.expect(res).to.be.a('array');
		chai.expect(res[0]).to.be.false;
		chai.expect(res[1]).to.be.false;
		chai.expect(res[2]).to.be.true;
	});
	
	it ("should check luhn, fail undefined card type", function () {
		var res = isValidCard("Robocop")(4242424242424242, 123);
		chai.expect(res).to.be.false;
	});
	
	it ("should check luhn, check Visa", function () {
		var res = isValidCard("Visa")(4242424242424242, 123);
		chai.expect(res).to.be.a('array');
		chai.expect(res[0]).to.be.true;
		chai.expect(res[1]).to.be.true;
	});
	
	it ("should check luhn, check Visa, fail cvv", function () {
		var res = isValidCard("Visa")(4242424242424242, 1234);
		chai.expect(res).to.be.a('array');
		chai.expect(res[0]).to.be.true;
		chai.expect(res[1]).to.be.true;
		chai.expect(res[2]).to.be.false;
	});
	
	it ("should check dirty string", function () {
		var res = isValidCard("Visa")("4242 4242 4242 4242", "wholala!456");
		chai.expect(res).to.be.a('array');
		chai.expect(res[0]).to.be.true;
		chai.expect(res[1]).to.be.true;
	});
});




































