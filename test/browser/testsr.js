define('TestsRequire', ["CardValidator"], function (CardValidator) {
	return {
		run: function () {
			QUnit.test( "Use Card Validator", function ( assert ) {
				assert.ok(CardValidator !== undefined, "CardValidator defined");
				assert.ok(CardValidator !== null, "CardValidator is not null");
				assert.equal(CardValidator.firstMatch("4242424242424242"), "Visa", "Visa test");
				assert.equal(CardValidator.firstMatch("4913327686655684"), "Visa Electron", "Visa Electron");
				assert.equal(CardValidator.firstMatch("5590425946410606"), "Mastercard", "Mastercard");
				assert.equal(CardValidator.extractUSZip("11022"), "11022", "11022");
				assert.equal(CardValidator.extractCanadaPostal("x0a!1A1"), "X0A1A1", "X0A1A1");
			});
		}
	};
});