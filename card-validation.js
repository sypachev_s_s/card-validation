﻿(function (undefined) {
    var CardValidatorX = (function (undefined) {
        var self = this;

        self.cvv3Digit = function (str) {
            return !!str.match('^\\d{3}$');
        };
        self.cvv4Digit = function (str) {
            return !!str.match('^\\d{4}$');
        };
        self.CanadaZipCodeFirstLetters  = "[ABCEGHJKLMNPRSTVXY]";
        self.CanadaZipCodeOtherLetters  = "[ABCEGHJKLMNPRSTVWXYZ]";
        self.CanadaZipCodeRegexp = "^" + self.CanadaZipCodeFirstLetters +
                                    "[0-9]" + self.CanadaZipCodeOtherLetters +
                                    "[0-9]" + self.CanadaZipCodeOtherLetters + "[0-9]$";

        self.getAlphaNums = function (str) {
            var i = 0,
                len = str.length || 0,
                out = [""],
                ch;
            for (; i < len; i += 1) {
                ch = str.charAt(i);
                if ((ch >= '0' && ch <= '9') || (ch >= 'A' && ch <= 'z')) {
                    out.push(ch);
                }
            }
            return out.join("");
        };

        self._matchCanadaPostal = function (str) {
            return str.match(CanadaZipCodeRegexp);
        };

        self.matchCanadaPostal = function (num) {
            if (typeof(num) !== 'string') {
                return false;
            }
            return !!self._matchCanadaPostal(self.getAlphaNums(num).toUpperCase());
        };

        self.cardsRegexp = {
            'Visa Electron' : {
                name: 'Visa Electron',
                family: 'Visa',
                aka: [],
                //4026, 417500, 4508, 4844, 4913, 4917, 16-d
                vn: function (str) {
                    return str.match('^4(026|508|844|91(3|7))[0-9]{12}|(^417500[0-9]{10})$');
                },
                vc: cvv3Digit
            },
            'Visa' : {
                name: 'Visa',
                family: 'Visa',
                aka: [],
                vn: function (str) {
                    return str.match('^4[0-9]{12,15}$');
                },
                vc: cvv3Digit
            },
            'Discover' : {
                name: 'Discover',
                family: "Discover",
                aka: [],
                //6011, 622126 to 622925, 644, 645, 646, 647, 648, 649, 65, 16-d
                vn: function (str) {
                    var sw = parseInt(str.substr(0, 6));
                    if (sw > 622125 && sw < 622926) {
                        return str.match('^622[0-9]{13}');
                    }
                    return str.match('(^6011[0-9]{12}$)|(^64[4-9][0-9]{13}$)|(^65[0-9]{14}$)');
                },
                vc: cvv3Digit
            },
            'American Express' : {
                //34, 37, 15-d
                name: 'American Express',
                family: "American Express",
                aka: [],
                vn: function (str) {
                    return str.match('^3[47][0-9]{13}$')
                },
                vc: cvv4Digit
            },
            'Diners Club - Carte Blanch' : {
                name: 'Diners Club - Carte Blanch',
                family: "Diners Club",
                aka: [],
                //300, 301, 302, 303, 304, 305, 14-d
                vn: function (str) {
                    return str.match('^30[0-5][0-9]{11}$');
                },
                vc: cvv3Digit
            },
            'Diners Club - International' : {
                name: 'Diners Club - International',
                family: "Diners Club",
                aka: [],
                //36, 14-d
                vn: function (str) {
                    return str.match('(^36[0-9]{12}$)|(^3[4,8,9][0-9]{14}$)');
                },
                vc: cvv3Digit
            },
            'JCB' : {
                name: 'JCB',
                family: "JCB",
                aka: [],
                vn: function (str) {
                    //3528 to 3589, 16-d
                    var sw = parseInt(str.substr(0, 4));
                    if (sw > 3527 && sw < 3590) {
                        return str.match('^35[0-9]{14}$');
                    }
                    return false;
                },
                vc: cvv3Digit
            },
            'Laser' : {
                name: 'Laser',
                family: 'Visa',
                aka: [],
                //6304, 6706, 6771, 6709, 16-19-d
                vn: function (str) {
                    return str.match('^(6304|67(06|71|09))[0-9]{12,15}$');
                },
                vc: cvv3Digit
            },
            'Maestro' :  {
                name: 'Maestro',
                family: "Maestro",
                aka: [],
                //5018, 5020, 5038, 5893, 6304, 6759, 6761, 6762, 6763, 16-19-d
                vn: function (str) {
                    return str.match('^(5018|5020|5038|5893|6304|6759|6761|6762|6763)[0-9]{12,15}');
                },
                vc: cvv3Digit
            },
            'Visa Dankort' : {
                name: 'Visa Dankort',
                family: 'Visa',
                aka: [],
                //5019, 16d
                vn: function (str) {
                    return str.match('^5019[0-9]{12}');
                },
                vc: cvv3Digit
            },
            'UATP' : {
                name: 'UATP',
                family: "UATP",
                aka: [],
                vn:    function (str) {
                    return str.match('^1[0-9]{14}$');
                },
                vc: cvv3Digit
            },
            'UnionPay' : {
                name: 'UnionPay',
                family: "UnionPay",
                aka: ['China UnionPay'],
                vn: function (str) {
                    return str.match('^62[0-9]{14}$');
                },
                vc: cvv3Digit
            },
            'Mastercard' : {
                name: 'Mastercard', //Eurocard
                family: "Mastercard",
                aka: ["Eurocard"],
                //51, 52, 53, 54, 55, 2221-2720, 16-d
                vn: function (str) {
                    var sw = parseInt(str.substr(0, 4));
                    if (sw > 2220 && sw < 2721) {
                        return str.length === 16;
                    }
                    return str.match('^5[1-5][0-9]{14}$');
                },
                vc: cvv3Digit
            },
            'MIR' : {
                name: 'MIR',
                family: "MIR",
                aka: ['NSPK MIR'],
                //2200 - 2204, 16-d
                vn: function (str) {
                    return str.match('^220[0-4][0-9]{12}$');
                },
                vc: cvv3Digit
            },
            'InterPayment' : {
                name: 'InterPayment',
                family: "InterPayment",
                aka: [],
                //636, 16-19-d
                vn: function (str) {
                    return str.match('^636[0-9]{13,16}$');
                },
                vc: cvv3Digit
            }
        };

        self.cardsOrder = [
            'Visa Electron',
            'Visa',
            'Discover',
            'American Express',
            'Diners Club - Carte Blanch',
            'Diners Club - International',
            'JCB',
            'Laser',
            'Maestro',
            'Visa Dankort',
            'UATP',
            'UnionPay',
            'Mastercard',
            'MIR',
            'InterPayment'
        ];

        self.isNumericString = function (arg) {
            if (typeof(arg) !== 'string') {
                return false;
            }
            return !!arg.match('^\\d+$');
        }

        self.getNums = function (str) {
            var i = 0,
                len = str.length || 0,
                out = [""],
                ch;
            for (; i < len; i += 1) {
                ch = str.charAt(i);
                if (ch >= '0' && ch <= '9') {
                    out.push(ch);
                }
            }
            return out.join("");
        };

        self.getAlphaNumsHyphen = function (str) {
            var i = 0,
                len = str.length || 0,
                out = [""],
                ch;
            for (; i < len; i += 1) {
                ch = str.charAt(i);
                if ((ch >= '0' && ch <= '9') || ch === '-') {
                    out.push(ch);
                }
            }
            return out.join("");
        };

        /**
            Returns function that checks argument against type
            @param type - name of card type, keys of `cards` object
            @return [Function] - function that takes card number argument which returns [Array] - array
            of two elements, first element is Luhn test, second is test against type
            @example isValidCard('Visa')("4242424242424242") -> [true,   true] - Luhn true, Visa true
            @example isValidCard('Visa')("4242424242424243") -> [false,  true] - Luhn false, Visa true
            @example isValidCard('Visa')("3242424242424244") -> [true,  false] - Luhn true, but not Visa
            @example isValidCard('Visa')("3242424242424243") -> [false, false] - Luhn false, and not Visa
        */
        self.isValidCard = function (type) {
            return function (cardNumber, cvv) {
                var crr, out;
                if (!self.cardsRegexp.hasOwnProperty(type)) {
                    return false;
                }
                if (cardNumber === undefined || cardNumber === null) {
                    cardNumber = "";
                }
                if (typeof(cardNumber) !== 'string') {
                    cardNumber = cardNumber.toString();
                }
                out = [self.Luhn(cardNumber), !!self.cardsRegexp[type].vn(self.getNums(cardNumber))];
                if (cvv === undefined || cvv === null) {
                    return out;
                } else {
                    if (typeof(cvv) !== 'string') {
                        cvv = cvv.toString();
                    }
                    out.push(!!self.cardsRegexp[type].vc(self.getNums(cvv)));
                    return out;
                }
            }
        };

        self._matchUSZip = function (str) {
            return str.match("^[0-9]{5}(?:-[0-9a-zA-Z]{4})?$");
        };

        self.SHOE = "10022-SHOE";

        self.matchUSZip = function (num) {
            if (num === undefined || num === null) {
                return false;
            }
            if (typeof(num) !== 'string') {
                num = num.toString();
            }
            if (num.replace(/[ ]/g, '').toUpperCase() === self.SHOE) {
                return true;
            }
            return !!self._matchUSZip(self.getAlphaNumsHyphen(num));
        };

        self.extractUSZip = function (zip) {
            if (zip.replace(/[^102\-SHOE]/gi, '').toUpperCase() === self.SHOE) {
                return self.SHOE;
            }
            return self.getAlphaNumsHyphen(zip);
        };

        self.extractCanadaPostal = function (postal) {
            return self.getAlphaNums(postal).toUpperCase();
        };

        self._firstMatch = function (str) {
            var i,
                len = self.cardsOrder.length,
                name;
            for (i = 0; i < len; i += 1) {
                name = self.cardsOrder[i];
                if (self.cardsRegexp[name].vn(str)) {
                    return name;
                }
            }
            return false;
        };

        /**
            Returns first matching number
            @param str - carn number string
            @return string, the key for `cards` object. Returns false if didn't match anything
            or argument is not a string
        */
        self.firstMatch = function (str) {
            if (typeof(str) !== 'string') {
                return false;
            }
            return self._firstMatch(self.getNums(str));
        }

        /**
            Inner implementation, the algorithm itself
            @param num - clean numeric string
            @return true or false
        */
        self._Luhn = function (num) {
            var chars       = num.split('').map(function (item) { return parseInt(item); }),
                lenbutone   = chars.length - 1,
                sum         = 0,
                evenRes     = chars.length % 2,
                i;
            for (i = 0; i < lenbutone; i += 1) {
                if (i % 2 === evenRes) {
                    chars[i] *= 2;
                    if (chars[i] > 9) {
                        chars[i] -= 9;
                    }
                }
                sum += chars[i];
            }
            return (sum + chars[lenbutone]) % 10 === 0;
        };

        /**
            Check `num` against num algorithm
            @param num - number or string, containing number. Luhn extracts
            numbers from string
            @return boolean true or false
        */
        self.Luhn = function (num) {
            if (num === undefined || num === null) {
                return false;
            }
            if (typeof(num) !== 'string') {
                num = num.toString();
            }
            num = self.getNums(num);
            if (num === "") {
                return false;
            }
            return self._Luhn(num);
        };

        self.out = {
            luhn:                   self.Luhn,
            getNums:                self.getNums,
            firstMatch:             self.firstMatch,
            isValidCard:            self.isValidCard,
            matchUSZip:             self.matchUSZip,
            extractUSZip:           self.extractUSZip,
            extractCanadaPostal:    self.extractCanadaPostal,
            matchCanadaPostal:      self.matchCanadaPostal,
            cards:                  self.cardsRegexp,
            cardsOrder:             self.cardsOrder
        };

        if (typeof module !== 'undefined' && typeof module.exports !== 'undefined') {
            self.out['internals'] = {
                _matchCanadaPostal    : self._matchCanadaPostal,
                matchCanadaPostal     : self.matchCanadaPostal,
                matchUSZip            : self.matchUSZip,
                _matchUSZip           : self._matchUSZip,
                luhn                  : self.Luhn,
                getNums               : self.getNums,
                _firstMatch           : self._firstMatch,
                firstMatch            : self.firstMatch,
                isValidCard           : self.isValidCard,
                cards                 : self.cardsRegexp,
                isNumericString       : self.isNumericString,
                cardsRegexp           : self.cardsRegexp,
                cardsOrder            : self.cardsOrder,
                extractUSZip          : self.extractUSZip,
                extractCanadaPostal   : self.extractCanadaPostal,
            };
        }

        return self.out;
    })();

    if (typeof module !== 'undefined' && typeof module.exports !== 'undefined') {
        module.exports._matchCanadaPostal    = CardValidatorX.internals._matchCanadaPostal;
        module.exports.matchCanadaPostal     = CardValidatorX.internals.matchCanadaPostal;
        module.exports.matchUSZip            = CardValidatorX.internals.matchUSZip;
        module.exports._matchUSZip           = CardValidatorX.internals._matchUSZip;
        module.exports.luhn                  = CardValidatorX.internals.luhn;
        module.exports.getNums               = CardValidatorX.internals.getNums;
        module.exports._firstMatch           = CardValidatorX.internals._firstMatch;
        module.exports.firstMatch            = CardValidatorX.internals.firstMatch;
        module.exports.isValidCard           = CardValidatorX.internals.isValidCard;
        module.exports.cards                 = CardValidatorX.internals.cardsRegexp;
        module.exports.isNumericString       = CardValidatorX.internals.isNumericString;
        module.exports.cardsRegexp           = CardValidatorX.internals.cardsRegexp;
        module.exports.cardsOrder            = CardValidatorX.internals.cardsOrder;
        module.exports.extractUSZip          = CardValidatorX.internals.extractUSZip;
        module.exports.extractCanadaPostal   = CardValidatorX.internals.extractCanadaPostal;

        module.exports.CardValidator         = CardValidatorX;
    } else {
        if (typeof define === 'function' && define.amd) {
            define([], function() {
                return CardValidatorX;
            });
        } else {
            window.CardValidator = CardValidatorX;
        }
    }
})();

