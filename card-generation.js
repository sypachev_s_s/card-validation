(function (undefined) {
	
	var CardGenerator = (function () {
		var self = this;
		self.randNumsArr = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '0'];
		self.genRandNumString = function (len) {
			var i, out = [];
			for (i = 0; i < len; i += 1) {
				out.push(Math.floor(Math.random() * 11));
			}
			return out.join("");
		};

		self.preLuhn = function (num) {
			var chars 		= num.split('').map(function (item) { return parseInt(item); }),
				lenbutone	= chars.length,
				sum 		= 0,
				evenRes 	= (chars.length + 1) % 2,
				i;
			for (i = 0; i < lenbutone; i += 1) {
				if (i % 2 === evenRes) {
					chars[i] *= 2;
					if (chars[i] > 9) {
						chars[i] -= 9;
					}
				}
				sum += chars[i];
			}
			return (10 - (sum % 10)) % 10;
		};
		
		self.gen3Cvv = function () {
			return self.genRandNumString(3);
		};
		
		self.genCardNumber = function (len) {
			
		};
		
		self.genCards = {
			'Visa Electron': {
				genNumber: function () {
				},
				genCvv: self.gen3Cvv
			}
		};
		
		self.cardsOrder = [
            'Visa Electron',
            'Visa',
            'Discover',
            'American Express',
            'Diners Club - Carte Blanch',
            'Diners Club - International',
            'JCB',
            'Laser',
            'Maestro',
            'Visa Dankort',
            'UATP',
            'UnionPay',
            'Mastercard',
            'MIR',
            'InterPayment'
        ];
		
		return {
			genRandNumString: 	self.genRandNumString,
			cardsOrder:			self.cardsOrder,
			preLuhn:			self.preLuhn
		};
		
	})();
	
	if (typeof module !== 'undefined' && typeof module.exports !== 'undefined') {
        module.exports.CardGenerator = CardGenerator;
    } else {
        if (typeof define === 'function' && define.amd) {
            define([], function() {
                return CardGenerator;
            });
        } else {
            window.CardGenerator = CardGenerator;
        }
    }
	
})();