﻿# Card Validation Library

Please, refer to repository [homepage](https://bitbucket.org/sypachev_s_s/card-validation#readme), to avoid possible
misspellings.

Library validates card number, gets type of card issuer (Visa, Discover, etc), validates CVV, validates US zipcodes and 
Canada Postal Numbers.

Card validator supports (current version 1.0.9)

- American Express
- Diners Club - Carte Blanch
- Diners Club - International
- Discover
- JCB
- InterPayment
- Laser
- Maestro
- Mastercard
- MIR
- Visa 
- Visa Dankort
- Visa Electron
- UATP
- UnionPay

# Usecase

Installation

    npm install card-verification --save
	
If you want to use library in browser, refer to

	<script src="../node_modules/card-verification/card-validation.js"></script>

Main object - `CardValidator`, contains several objects to verify cards

    var CardValidator = require("card-verification").CardValidator;
	
# Methods

### Boolean `CardValidator.luhn` ( Number|String )

Implementation of [Luhn algorithm](https://en.wikipedia.org/wiki/Luhn_algorithm).
	
Returns `true` if argument matches, false if not, or argument has bad type (undefined, null, neither string nor number, string containing no numbers).
`luhn` authomatically extract numbers from string, so you can pass dirty strings.

	//true
    CardValidator.luhn("4242424242424242");
    CardValidator.luhn("4242 4242 4242 4242");
    CardValidator.luhn(4242424242424242);
    CardValidator.luhn("4242 oh my ... 424242424242🐹");
	
	//false
	CardValidator.luhn("4242424242424243");
    CardValidator.luhn("4242 4242 4242 4243");
    CardValidator.luhn(4242424242424243);
    CardValidator.luhn("4242 oh my ... 424242424243🐹");
	
### String `CardValidator.getNums` (String)

Returns all numbers from string argument

    //returns "1234567890"
	CardValidator.getNums("1234-56!.78*9👊💥0");
	
### String | Boolean `CardValidator.firstMatch` ( String )

Returns first matching type of card. Possible values are

- American Express
- Diners Club - Carte Blanch
- Diners Club - International
- Discover
- JCB
- InterPayment
- Laser
- Maestro
- Mastercard
- MIR
- Visa 
- Visa Dankort
- Visa Electron
- UATP
- UnionPay

If nothing was found, or argument was not a string, returns false.

**Note**: some cards have differen names but belong to one family (for example, Visa Electron is still Visa). To deal
with family, use `family` field of `Card Validator.cards` object.

**Note**: firstMatch does not check against Luhn algorithm, so cards with wrong numbers match too.

### Function `CardValidator.isValidCard` ( String cardType )

Returns function that validate card number and cvv against sent cardType argument. Anonymous function
returns array of booleans, or boolean.

    Array function (String|Number cardNumber, [optional String|Number cvv])

In case of unknown type returns false

	//returns false
    isValidCard("Robocop")("4242424242424242");
	
In case of valid card returns array, where first boolean is Luhn test, second is card type test

    //returns [true, true]
	CardValidator.isValidCard("Visa")("4242424242424242");
	
	//returns [false, true]
	CardValidator.isValidCard("Visa")("4242424242424243");
	
	//returns [true, false]
	CardValidator.isValidCard("Visa")("3242424242424244");
	
	//returns [false, false]
	CardValidator.isValidCard("Visa")("3242424242424243");
	
	//returns [true, true]
	CardValidator.isValidCard("Visa")(4242424242424242);
	
	//returns [false, false]
	CardValidator.isValidCard("Visa")();
	
One can also check dirty strings

    CardValidator.isValidCard("Visa")("4242 4242 4242 4242");
	
In case two arguments were sent, second one is interpreted as cvv, and return type contains three booleans: Luhn test, card number test and cvv test

    //returns [true, true, true]
    CardValidator.isValidCard("Visa")("4242424242424242", "123");
	
One can also send numbers or dirty strings as arguments

	//returns [true, true, true]
    CardValidator.isValidCard("Visa")("4242 4242 4242 4242", "wholala!456")
	
### CardValidator.cards

Main object, contains fields

- `name` - equals key of object
- `family` - general card type. For example, "Diners Club", "Diners Club - International" and "Diners Club - Enroute"
	both have family "Diners Club"
- `aka` - also known as, possible name of card. For example, Mastercard aka Eurocard
- `vn` - function that checks string representation of card number against card make
- `vc` - function that checks string representation of cvv


    console.log(CardValidator.cards);

### CardValidator.cardsOrder

Array of strings, contains all keys of `cards` object. Strings are order in a way they are
being tested. Because some cards have wider regexps than others, order matters.

### Boolean `CardValidator.matchUSZip` ( String | Number zipCode )

Matches US zip code. Returns false if doesn't match or bad argument was sent (undefined, null, neither string nor number).

Matches `xxxxx` or `xxxxx-xxxx` zip codes and extracts numeric symbols and hyphens. Also checks for '10022-SHOE'.

    //returns true
	CardValidator.matchUSZip("12345");
	CardValidator.matchUSZip(" 1 2 3 4 5 ");
	CardValidator.matchUSZip(" 1 2 3 4 5 - 34 67 ");
	CardValidator.matchUSZip(" 10022 - shoe "));
	CardValidator.matchUSZip(12345);
	CardValidator.matchUSZip("12345-1234");
	
	//returns false
	CardValidator.matchUSZip(02345);
	CardValidator.matchUSZip("12345 1234");
	CardValidator.matchUSZip("12345-1234-");
	
### Boolean `CardValidator.matchCanadaPostal` ( String )

Matches Canada Postal Code. Extracts alphanumeric symbols and uppercase them. Returns boolean result of test, or false, if bad argument was sent (not a string)

	//returns true
	CardValidator.matchCanadaPostal('X0A1A1');
	CardValidator.matchCanadaPostal(' X0A1A1 ');
	CardValidator.matchCanadaPostal(' X0A-1A1 ');
	CardValidator.matchCanadaPostal('X0A 1A1');
	CardValidator.matchCanadaPostal('x0a 1a1');
	CardValidator.matchCanadaPostal('x0a🌐1a1');
	
	//returns false
	CardValidator.matchCanadaPostal('1A11A1');
	CardValidator.matchCanadaPostal('Z0A1A1');
	CardValidator.matchCanadaPostal('X0A1A12');
	CardValidator.matchCanadaPostal('X011O1');
	CardValidator.matchCanadaPostal();
	CardValidator.matchCanadaPostal(null);
	CardValidator.matchCanadaPostal(123456);
	
### String `CardValidator.extractUSZip(String)`

Extracts uppercase zip code in format XXXXX or XXXXX-XXXX, including 10022-SHOE. Does not check if zip code is invalid!

	//10022
	extractUSZip("10022");
	extractUSZip(" 10022 ");
	extractUSZip(" 1x0x0x2x2 ")
	extractUSZip("😹10022!!!")

### String `CardValidator.extractCanadaPostal(String)`

Extracts uppercase Canada postal number. Does not check if zip code is invalid!

	//X0A1S1
    extractCanadaPostal("x0a1s1");
	//X0A1A1
    extractCanadaPostal("x0a-1A1");
    extractCanadaPostal("x0a 1A1");
    extractCanadaPostal(" x0a-1A1");
    extractCanadaPostal("x0a!1A1");
    extractCanadaPostal("x0a😰1A1");
	
# Dependencies

Project has no dependencies for product version. For testing issues requires `mocha`, `chai` & `randomstring`
	
# Test

Clone this repo for test

	git clone https://bitbucket.org/sypachev_s_s/card-validation --depth 1
	cd card-validation
	npm install
	npm run test
	
To test browser version, open `test/browser/index.html`, or `test/browser/require.html` (AMD)

# What's new

- **1.0.3** - MIR, InterPayment card support. enRoute removed as inactive issuer
- **1.0.5** - Mastercard 2017 - 2221-2720 IIN, Mastercard bug - only 16 digit card available
- **1.0.6** - bug with MIR and InterPayment cvv verification, add test to cover possible inconsistency
- **1.0.7** - Canada postal code validation
- **1.0.8** - Browser support, AMD
- **1.0.9** - extract zip code and postal from string


































